import { Editor } from "@ckeditor/ckeditor5-core";
import { EditorWithUI } from "@ckeditor/ckeditor5-core/src/editor/editorwithui";
import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import Model from "@ckeditor/ckeditor5-ui/src/model";
import Collection from "@ckeditor/ckeditor5-utils/src/collection";
import {
  addListToDropdown,
  createDropdown,
  ListDropdownItemDefinition,
} from "@ckeditor/ckeditor5-ui/src/dropdown/utils";

export default class TestPlugin extends Plugin {
  static get pluginName(): string {
    return "TestPlugin";
  }

  init(): void {
    const editor: Editor | EditorWithUI = this.editor;

    if (!("ui" in editor)) {
      return;
    }

    editor.ui.componentFactory.add("testPlugin", (locale: any) => {
      const dropdownView = createDropdown(locale);
      dropdownView.id = "ckTestPluginDropdown";
      dropdownView.class = "ck-test-plugin-dropdown";

      editor.on("change:isReadOnly", () => {
        dropdownView.isEnabled = !editor.isReadOnly;
      });

      dropdownView.buttonView.set({
        withText: true,
        // tooltip: true,
        label: "Custom dropdown",
      });

      const items: Collection<ListDropdownItemDefinition, "id"> =
        new Collection();

      items.add({
        type: "button",
        model: new Model({
          id: "foo",
          label: "Foo",
          withText: true,
        }),
      });

      items.add({
        type: "button",
        model: new Model({
          id: "bar",
          label: "Bar",
          withText: true,
        }),
      });

      addListToDropdown(dropdownView, items);

      dropdownView.on("execute", (e: any) => {
        const { id, label } = e.source;
        editor.fire("testPluginEvent", { id, label });
      });

      return dropdownView;
    });
  }
}
