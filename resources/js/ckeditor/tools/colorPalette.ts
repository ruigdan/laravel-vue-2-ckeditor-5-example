export default [
  {
    color: '#000000',
    label: 'Black',
  },
  {
    color: '#4d4d4d',
    label: 'Dim grey',
  },
  {
    color: '#999999',
    label: 'Grey',
  },
  {
    color: '#e6e6e6',
    label: 'Light grey',
  },
  {
    color: '#ffffff',
    label: 'White',
    hasBorder: true,
  },
  {
    color: '#ff0000',
    label: 'Red',
  },
  {
    color: '#e92063',
    label: 'Pink',
  },
  {
    color: '#e6994c',
    label: 'Orange',
  },
  {
    color: '#ffff00',
    label: 'Yellow',
  },
  {
    color: '#99e64c',
    label: 'Light green',
  },
  {
    color: '#00ff00',
    label: 'Green',
  },
  {
    color: '#4ce699',
    label: 'Aquamarine',
  },
  {
    color: '#4ce6e6',
    label: 'Turquoise',
  },
  {
    color: '#4c99e6',
    label: 'Light blue',
  },
  {
    color: '#0000ff',
    label: 'Blue',
  },
  {
    color: '#994ce6',
    label: 'Purple',
  },
  {
    color: '#673ab6',
    label: 'Deep Purple',
  },
];
